
import './App.css';
import { Input } from './Components/Input';
import { AuthButton } from './Components/AuthButton';

function App() {
  return (
    <div className="App">
            <div>
              <Input label = 'Login'/>
            </div>
            <div>
            <Input label = 'Password'/>
            </div>
            <div>
              <AuthButton/>
            </div>
      </div>

  );
}

export default App;
