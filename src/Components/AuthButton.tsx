import { Component, ReactNode } from "react";
import axios from "axios";

export class AuthButton extends Component {

    state = {
        persons: []
    }

    onAuth = () => {
        axios.get('/users').then((response) => {
            const data = response.data
            console.log(data)
            this.setState({data})
        })
    }

    render(): ReactNode {
        return <button onClick={this.onAuth}>
            Авторизоваться и получить список пользователей
        </button>
    }
}