// import { Component, ReactNode } from "react";

// export class Input extends Component<Props> {
//     render(): ReactNode {
//         return <label>
//             Text input: <input name="myInput" />
//         </label>
//     }
// }

interface InputProps{
    label: string;
}

export function Input(props:InputProps) {
    return <label>
        {props.label}: <input name="myInput" />
    </label>
}
